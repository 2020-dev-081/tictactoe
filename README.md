**Tic Tac Toe**

**About the project**

This is a TicTacToe iOS app written in Swift and without using any external libraries.


---

**How to run the app**


Clone and open the project with Xcode. Please make sure that you have a new version of Xcode, preferably Xcode 11.2 or newer.

Select the simulator on which you want to run the app, make sure it runs on iOS 13. The app works on both iPhone and iPad, on big or a small screen.

Then build and run the project **cmd + R**

