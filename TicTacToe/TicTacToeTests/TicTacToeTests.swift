import XCTest
@testable import TicTacToe

class TicTacToeTests: XCTestCase {

    private var board: Board!
    
    
    override func setUpWithError() throws {
        // Test 1: reset the board to a new instance before every test
        self.board = Board()
    }

    func testInitialState() {
        
        XCTAssertEqual(board.state, Board.State.playing(.cross))
    }
    
    func testPlayFirstMove() {
        
        self.board.play(player: .cross, at: 0)
        XCTAssertEqual(board.state, Board.State.playing(.nought))
    }

    func testPlaySecondMove() {

        self.board.play(player: .cross, at: 0)
        XCTAssertEqual(board.playerBoard[0], Board.Player.cross)
    }
        
    func testPlayTwoMoves() {
        
        self.board.play(player: .cross, at: 0)
        self.board.play(player: .nought, at: 1)
        
        XCTAssertEqual(board.state, Board.State.playing(.cross))
    }
    
    func testPlayWinState() {
                
        self.board.play(player: .cross, at: 0)
        self.board.play(player: .nought, at: 3)
        self.board.play(player: .cross, at: 1)
        self.board.play(player: .nought, at: 4)
        self.board.play(player: .cross, at: 2)
        
        XCTAssertEqual(board.state, Board.State.won(.cross))
    }
    
    func testPlayDrawState() {
                
        self.board.play(player: .cross, at: 2)
        self.board.play(player: .nought, at: 0)
        self.board.play(player: .cross, at: 3)
        self.board.play(player: .nought, at: 5)
        self.board.play(player: .cross, at: 4)
        self.board.play(player: .nought, at: 6)
        self.board.play(player: .cross, at: 8)
        self.board.play(player: .nought, at: 7)
        self.board.play(player: .cross, at: 1)
        
        XCTAssertEqual(board.state, Board.State.draw)
    }

    
}
