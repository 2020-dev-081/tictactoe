import Foundation

protocol BoardDelegate {
    func gameOver(_ player: Board.Player, state: Board.State)
    func markPlayed(mark: Board.Player, at position: Int)
}

class Board {
    
    // Mark :- private properties
    
    private(set) var state: State
    private(set) var player: Player
    private(set) var winner: Player
    private(set) var playerBoard = Array(repeating: Player.empty, count: 9)
    private var availablePositions: [Int] {
        return playerBoard
            .indices
            .filter { playerBoard[$0] == .empty }
    }
    
    private let winningPattern = [
        
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        
        [0, 4, 8],
        [2, 4, 6]
        
        ].map(Set.init)
    
    // Mark :- public properties
    
    var delegate: BoardDelegate?

    // Mark :- init
    
    init() {
        self.state = .playing(.cross)
        self.player = .cross
        self.winner = .empty
    }
    
    // Mark :- actions
    
    func play(player: Player, at cell: Int?) {
        guard let cell = cell else { return }
        self.playerBoard[cell] = self.player
        self.setStateFor(player, at: cell)
    }
    
    func playRandom(_ player: Player = .nought) {
        guard let cell = availablePositions.randomElement() else { return }
        self.playerBoard[cell] = player
        self.setStateFor(player, at: cell)
    }
    
    func resetBoard() {
        self.playerBoard = Array(repeating: Player.empty, count: 9)
    }
    
    // Mark :- private functions
    
    private func setStateFor(_ player: Player, at position: Int) {
        
        self.state.setState()
        
        self.delegate?.markPlayed(mark: player, at: position)
        
        let winnerState = self.checkWinnerFor(player)
        if winnerState {
            self.state = .won(player)
            self.winner = player
            self.delegate?.gameOver(player, state: .won(player))
        }
        
        if availablePositions.count == 0 {
            self.state = .draw
            self.delegate?.gameOver(player, state: .draw)
        }
    }
    
    private func checkWinnerFor(_ player: Player) -> Bool {
        
        let moveCurrentPlayer = Set(self.playerBoard.indices.filter {playerBoard[$0] == player })

        for winnerPattern in self.winningPattern {
            if winnerPattern.isSubset(of: moveCurrentPlayer) {
                return true
            }
        }
        
        return false
    }
}
