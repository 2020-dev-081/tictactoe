import UIKit

class BoardViewController: UIViewController {

    // MARK :- IBOutlets
    
    @IBOutlet private var cells: [UIButton]!
    @IBOutlet private weak var playerLabel: UILabel!  {
           didSet {
               self.playerLabel.text = "Start Playing ❌⭕️"
           }
       }
    
    // MARK :- Private properties
    
    private var board: Board!
    private var crossMark: UIImage!
    private var noughtMark: UIImage!
    
    // MARK :- View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setup()
    }
    
    // MARK :- Setup
    
    private func setup() {
        self.board = Board()
        self.board.delegate = self
        self.noughtMark = UIImage(named: "nought")
        self.crossMark = UIImage(named: "cross")
        self.cells.forEach { $0.setImage(nil, for: .normal) }
        self.view.isUserInteractionEnabled = true
    }

    // MARK :- IBActions
    
    @IBAction private func didTapOn(cell: UIButton) {
        let index = cells.firstIndex(of: cell)
        self.board.play(player: self.board.player, at: index)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if self.board.state != .won(self.board.winner) {
                self.board.playRandom()
            }
        }
    }
}

extension BoardViewController: BoardDelegate {
    
    // MARK :- Delegate
    
    func markPlayed(mark: Board.Player, at position: Int) {
        
        if mark == .nought {
            self.cells[position].setImage(self.noughtMark, for: .normal)
            self.cells[position].isUserInteractionEnabled = false
        }
        
        if mark == .cross {
            self.cells[position].setImage(self.crossMark, for: .normal)
            self.cells[position].isUserInteractionEnabled = false
        }
        
        self.playerLabel.text = "\(mark.rawValue.uppercased())"
    }
    
    func gameOver(_ player: Board.Player, state: Board.State) {

        if state == .won(player) {
            self.playerLabel.text = "\(player.rawValue.uppercased()) Won!! 🎉"
            self.board.resetBoard()
        } else if state == .draw {
            self.playerLabel.text = "Draw!! ☹️"
            self.board.resetBoard()
        }
        
        self.view.isUserInteractionEnabled = false
    }
}
