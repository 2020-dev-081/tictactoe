import Foundation

extension Board {
    
    enum State {
        case playing(Player)
        case won(Player)
        case draw
        
        mutating func setState() {
            guard case .playing(let mark) = self else { return }
            self = mark == .cross ? .playing(.nought) : .playing(.cross)
        }
    }
}

extension Board.State: Equatable {
    static func == (lhs: Board.State, rhs: Board.State) -> Bool {
        switch (lhs, rhs) {
        case let (.playing(mark1), .playing(mark2)):
            return mark1 == mark2
        case let (.won(mark1), .won(mark2)):
            return mark1 == mark2
        case (.draw, .draw):
            return true
        default: return false
        }
    }
}
