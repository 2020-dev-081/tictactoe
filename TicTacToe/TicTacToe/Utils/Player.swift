import Foundation

extension Board {
    
    enum Player: String {
        case empty
        case nought
        case cross
        
        var sign: String {
            switch self {
            case .empty: return " "
            case .nought: return "O"
            case .cross: return "X"
            }
        }
    }
}
